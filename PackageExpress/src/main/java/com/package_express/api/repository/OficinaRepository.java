package com.package_express.api.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.package_express.api.entity.Oficina;


@Repository
public interface OficinaRepository extends CrudRepository<Oficina, Integer> {

}
