package com.package_express.api.repository;

import org.springframework.data.repository.CrudRepository;

import com.package_express.api.entity.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {

}
