package com.package_express.api.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.package_express.api.entity.Estatus;

@Repository
public interface EstatusRepository extends CrudRepository<Estatus, Integer>{

}
