package com.package_express.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.package_express.api.service.OficinaService;

@SpringBootApplication
public class PackageExpressApplication implements CommandLineRunner {

	private OficinaService ofiServ;

	@Autowired
	public void ofiServ(OficinaService ofiServ) {
		this.ofiServ = ofiServ;
	}

	public static void main(String[] args) {
		SpringApplication.run(PackageExpressApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Server up and running...");
	
		System.out.println("Action done ...");
		
	}

}
