package com.package_express.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Estatus implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue (strategy = GenerationType.AUTO)
	@Column(name = "est_id")
	private Integer estId;
	@Column(name = "est_descripcion")
	private String estDescripcion;
	
	public Estatus() {
		super();
	}
	public Estatus(Integer estId, String estDescripcion) {
		super();
		this.estId = estId;
		this.estDescripcion = estDescripcion;
	}
	public Integer getEstId() {
		return estId;
	}
	public void setEstId(Integer estId) {
		this.estId = estId;
	}
	public String getEstDescripcion() {
		return estDescripcion;
	}
	public void setEstDescripcion(String estDescripcion) {
		this.estDescripcion = estDescripcion;
	}	
}
