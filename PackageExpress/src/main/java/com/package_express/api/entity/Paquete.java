package com.package_express.api.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Paquete implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "paq_id")
	private Integer paqId;
	@Column(name = "ofi_id")
	private Oficina oficina;
	@Column(name = "ofi_id_dest")
	private Oficina oficinaDest;
	@Column(name = "est_id")
	private Estatus estatus;
	@Column(name = "usu_id")
	private Usuario usuario;
	@Column(name = "paq_emi_nombre")
	private String paqEmiNombre;
	@Column(name = "paq_emi_apellido")
	private String paqEmiApellido;
	@Column(name = "paq_emi_telefono")
	private String paqEmiTelefono;
	@Column(name = "paq_emi_correo")
	private String paqEmiCorreo;
	@Column(name = "paq_des_nombre")
	private String paqDesNombre;
	@Column(name = "paq_des_apellido")
	private String paqDesApellido;
	@Column(name = "paq_des_telefono")
	private String paqDesTelefono;
	@Column(name = "paq_des_correo")
	private String paqDesCorreo;
	@Column(name = "paq_descripcion")
	private String paqDescripcion;
	@Column(name = "paq_peso")
	private Long paqPeso;
	@Column(name = "paq_precio")
	private Long paqPrecio;
	@Column(name = "paq_dimensiones")
	private Long paqDimensiones;
	@Column(name = "paq_valor")
	private Long paqValor;
	@Column(name = "paq_seguro")
	private boolean paqSeguro;
	@Column(name = "paq_notas")
	private String paqNotas;
	@Column(name = "paq_fecha_registro")
	private Date paqFechaRegistro;
	@Column(name = "paq_fecha_entrega")
	private Date paqFechaEntrega;
	@Column(name = "paq_ultima_mod")
	private Date paqUltimaMod;
	@OneToMany(mappedBy = "paquete")
	private List<TrackingHistory> trackingHistoryList;

	public Paquete() {
		super();
	}

	public Integer getPaqId() {
		return paqId;
	}

	public void setPaqId(Integer paqId) {
		this.paqId = paqId;
	}

	public Oficina getOfiId() {
		return oficina;
	}

	public void setOfiId(Oficina ofiId) {
		this.oficina = ofiId;
	}

	public Oficina getOfiIdDest() {
		return oficinaDest;
	}

	public void setOfiIdDest(Oficina ofiIdDest) {
		this.oficinaDest = ofiIdDest;
	}

	public Estatus getEstId() {
		return estatus;
	}

	public void setEstId(Estatus estId) {
		this.estatus = estId;
	}

	public Usuario getUsuId() {
		return usuario;
	}

	public void setUsuId(Usuario usuId) {
		this.usuario = usuId;
	}

	public String getPaqEmiNombre() {
		return paqEmiNombre;
	}

	public void setPaqEmiNombre(String paqEmiNombre) {
		this.paqEmiNombre = paqEmiNombre;
	}

	public String getPaqEmiApellido() {
		return paqEmiApellido;
	}

	public void setPaqEmiApellido(String paqEmiApellido) {
		this.paqEmiApellido = paqEmiApellido;
	}

	public String getPaqEmiTelefono() {
		return paqEmiTelefono;
	}

	public void setPaqEmiTelefono(String paqEmiTelefono) {
		this.paqEmiTelefono = paqEmiTelefono;
	}

	public String getPaqEmiCorreo() {
		return paqEmiCorreo;
	}

	public void setPaqEmiCorreo(String paqEmiCorreo) {
		this.paqEmiCorreo = paqEmiCorreo;
	}

	public String getPaqDesNombre() {
		return paqDesNombre;
	}

	public void setPaqDesNombre(String paqDesNombre) {
		this.paqDesNombre = paqDesNombre;
	}

	public String getPaqDesApellido() {
		return paqDesApellido;
	}

	public void setPaqDesApellido(String paqDesApellido) {
		this.paqDesApellido = paqDesApellido;
	}

	public String getPaqDesTelefono() {
		return paqDesTelefono;
	}

	public void setPaqDesTelefono(String paqDesTelefono) {
		this.paqDesTelefono = paqDesTelefono;
	}

	public String getPaqDesCorreo() {
		return paqDesCorreo;
	}

	public void setPaqDesCorreo(String paqDesCorreo) {
		this.paqDesCorreo = paqDesCorreo;
	}

	public String getPaqDescripcion() {
		return paqDescripcion;
	}

	public void setPaqDescripcion(String paqDescripcion) {
		this.paqDescripcion = paqDescripcion;
	}

	public Long getPaqPeso() {
		return paqPeso;
	}

	public void setPaqPeso(Long paqPeso) {
		this.paqPeso = paqPeso;
	}

	public Long getPaqPrecio() {
		return paqPrecio;
	}

	public void setPaqPrecio(Long paqPrecio) {
		this.paqPrecio = paqPrecio;
	}

	public Long getPaqDimensiones() {
		return paqDimensiones;
	}

	public void setPaqDimensiones(Long paqDimensiones) {
		this.paqDimensiones = paqDimensiones;
	}

	public Long getPaqValor() {
		return paqValor;
	}

	public void setPaqValor(Long paqValor) {
		this.paqValor = paqValor;
	}

	public boolean isPaqSeguro() {
		return paqSeguro;
	}

	public void setPaqSeguro(boolean paqSeguro) {
		this.paqSeguro = paqSeguro;
	}

	public String getPaqNotas() {
		return paqNotas;
	}

	public void setPaqNotas(String paqNotas) {
		this.paqNotas = paqNotas;
	}

	public Date getPaqFechaRegistro() {
		return paqFechaRegistro;
	}

	public void setPaqFechaRegistro(Date paqFechaRegistro) {
		this.paqFechaRegistro = paqFechaRegistro;
	}

	public Date getPaqFechaEntrega() {
		return paqFechaEntrega;
	}

	public void setPaqFechaEntrega(Date paqFechaEntrega) {
		this.paqFechaEntrega = paqFechaEntrega;
	}

	public Date getPaqUltimaMod() {
		return paqUltimaMod;
	}

	public void setPaqUltimaMod(Date paqUltimaMod) {
		this.paqUltimaMod = paqUltimaMod;
	}

}
