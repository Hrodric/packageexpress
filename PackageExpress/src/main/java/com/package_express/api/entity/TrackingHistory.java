package com.package_express.api.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

@Entity

public class TrackingHistory implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue (strategy = GenerationType.AUTO)
	@Column(name ="trc_id")
	private Integer trcId;

    @MapsId("paqId")
    @ManyToOne
	private Paquete paquete;
	@Column(name ="usu_id")
	private Usuario ususuario;
	@Column(name ="ofi_id")
	private Oficina oficina;
	@Column(name ="trc_notas")
	private String trcNotas;
	@Column(name ="trc_fecha_event")
	private Date trcFechaEvent;
	
	public TrackingHistory() {
		super();
	}

	public TrackingHistory(Integer trcId, Paquete paqId, Usuario usuId, Oficina ofiId, String trcNotas,
			Date trcFechaEvent) {
		super();
		this.trcId = trcId;
		this.paquete = paqId;
		this.ususuario = usuId;
		this.oficina = ofiId;
		this.trcNotas = trcNotas;
		this.trcFechaEvent = trcFechaEvent;
	}

	public Integer getTrcId() {
		return trcId;
	}

	public void setTrcId(Integer trcId) {
		this.trcId = trcId;
	}

	public Paquete getPaqId() {
		return paquete;
	}

	public void setPaqId(Paquete paqId) {
		this.paquete = paqId;
	}

	public Usuario getUsuId() {
		return ususuario;
	}

	public void setUsuId(Usuario usuId) {
		this.ususuario = usuId;
	}

	public Oficina getOfiId() {
		return oficina;
	}

	public void setOfiId(Oficina ofiId) {
		this.oficina = ofiId;
	}

	public String getTrcNotas() {
		return trcNotas;
	}

	public void setTrcNotas(String trcNotas) {
		this.trcNotas = trcNotas;
	}

	public Date getTrcFechaEvent() {
		return trcFechaEvent;
	}

	public void setTrcFechaEvent(Date trcFechaEvent) {
		this.trcFechaEvent = trcFechaEvent;
	}
	
	
}
