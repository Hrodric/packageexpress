package com.package_express.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Oficina implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ofi_id")
	private Integer ofiId;
	@Column(name = "ofi_pais")
	private String ofiPais;
	@Column(name = "ofi_estado")
	private String ofiEstado;
	@Column(name = "ofi_ciudad")
	private String ofiCiudad;
	@Column(name = "ofi_descripcion")
	private String ofiDescripcion;
	@Column(name = "ofi_zip_code")
	private Integer ofiZipCode;

	public Oficina() {
		super();
	}

	public Oficina(Integer ofiId, String ofiPais, String ofiEstado, String ofiCiudad, String ofiDescripcion,
			Integer ofiZipCode) {
		super();
		this.ofiId = ofiId;
		this.ofiPais = ofiPais;
		this.ofiEstado = ofiEstado;
		this.ofiCiudad = ofiCiudad;
		this.ofiDescripcion = ofiDescripcion;
		this.ofiZipCode = ofiZipCode;
	}

	public Integer getOfiId() {
		return ofiId;
	}

	public void setOfiId(Integer ofiId) {
		this.ofiId = ofiId;
	}

	public String getOfiPais() {
		return ofiPais;
	}

	public void setOfiPais(String ofiPais) {
		this.ofiPais = ofiPais;
	}

	public String getOfiEstado() {
		return ofiEstado;
	}

	public void setOfiEstado(String ofiEstado) {
		this.ofiEstado = ofiEstado;
	}

	public String getOfiCiudad() {
		return ofiCiudad;
	}

	public void setOfiCiudad(String ofiCiudad) {
		this.ofiCiudad = ofiCiudad;
	}

	public String getOfiDescripcion() {
		return ofiDescripcion;
	}

	public void setOfiDescripcion(String ofiDescripcion) {
		this.ofiDescripcion = ofiDescripcion;
	}

	public Integer getOfiZipCode() {
		return ofiZipCode;
	}

	public void setOfiZipCode(Integer ofiZipCode) {
		this.ofiZipCode = ofiZipCode;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Oficina[").append(this.getOfiId()).append(" - ").append(this.ofiPais).append(" - ")
				.append(this.ofiEstado).append(" - ").append(this.ofiCiudad).append(" - ").append(this.ofiZipCode)
				.append(" - ").append(this.ofiDescripcion).append("]");
		return sb.toString();
	}

}
