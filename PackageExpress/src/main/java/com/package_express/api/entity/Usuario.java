package com.package_express.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Usuario implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "usu_id")
	private Integer usuId;
	@Column(name = "usu_cve")
	private String usuCve;
	@Column(name = "usu_roll")
	private String usuRoll;
	@Column(name = "usu_estatus")
	private String usuEstatus;
	@Column(name = "usu_nombre")
	private String usuNombre;
	@Column(name = "usu_apellido")
	private String usuApellido;
	@Column(name = "ofi_id")
	private Oficina ofiId;
	@Column(name = "usu_pass")
	private String usuPass;
	
	public Usuario() {
		super();
	}

	public Usuario( String usuCve, String usuRoll, String usuEstatus, String usuNombre,
			String usuApellido, String usuPass, Oficina ofiId) {
		super();
		this.usuCve = usuCve;
		this.usuRoll = usuRoll;
		this.usuEstatus = usuEstatus;
		this.usuNombre = usuNombre;
		this.usuApellido = usuApellido;
		this.ofiId = ofiId;
		this.usuPass = usuPass;
	}

	public Integer getUsuId() {
		return usuId;
	}

	public void setUsuId(Integer usuId) {
		this.usuId = usuId;
	}

	public String getUsuCve() {
		return usuCve;
	}

	public void setUsuCve(String usuCve) {
		this.usuCve = usuCve;
	}

	public String getUsuRoll() {
		return usuRoll;
	}

	public void setUsuRoll(String usuRoll) {
		this.usuRoll = usuRoll;
	}

	public String getUsuEstatus() {
		return usuEstatus;
	}

	public void setUsuEstatus(String usuEstatus) {
		this.usuEstatus = usuEstatus;
	}

	public String getUsuNombre() {
		return usuNombre;
	}

	public void setUsuNombre(String usuNombre) {
		this.usuNombre = usuNombre;
	}

	public String getUsuApellido() {
		return usuApellido;
	}

	public void setUsuApellido(String usuApellido) {
		this.usuApellido = usuApellido;
	}

	public Oficina getOfiId() {
		return ofiId;
	}

	public void setOfiId(Oficina ofiId) {
		this.ofiId = ofiId;
	}

	public String getUsuPass() {
		return usuPass;
	}

	public void setUsuPass(String usuPass) {
		this.usuPass = usuPass;
	}

}
