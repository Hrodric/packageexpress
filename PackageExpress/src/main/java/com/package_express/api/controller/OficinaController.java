package com.package_express.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.package_express.api.entity.Oficina;
import com.package_express.api.interfaces.PackageExpressControllerInterface;
import com.package_express.api.service.OficinaService;

@RestController
@RequestMapping(path = "/rest/api/v1/oficina")
public class OficinaController implements PackageExpressControllerInterface<Oficina> {

	private OficinaService ofiService;

	@Autowired
	public void ofiService(OficinaService ofiService) {
		this.ofiService = ofiService;
	}

	@Override
	public Oficina get(Integer id) {
		return ofiService.get(id);
	}

	@Override
	public Oficina save(Oficina oficina) {
		return ofiService.save(oficina);
	}

	@Override
	public Oficina update(Oficina oficina) {
		return ofiService.update(oficina);
	}

	@Override
	public void delete(Integer id) {
		ofiService.delete(id);
	}

	@Override
	public List<Oficina> getAll() {
		return ofiService.getAll();
	}

}
