package com.package_express.api.controller.advice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.package_express.api.dto.MessageDTO;
import com.package_express.api.exception.ResourceNotFoundException;

@RestControllerAdvice
public class OficinaAdvice {

	@ExceptionHandler(ResourceNotFoundException.class)
	@ResponseBody
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public MessageDTO processResourceNotFoundException( ResourceNotFoundException e) {
		MessageDTO message = new MessageDTO();
		message.setMessage(e.getMessage());
		message.setType("NOT FOUND ERROR");
		return message;
	}
	
	@ExceptionHandler(NullPointerException.class)
	@ResponseBody
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public MessageDTO processNullPointerException(NullPointerException e) {
		MessageDTO message = new MessageDTO();
		message.setMessage( "Error trying to use a null variable");
		message.setType("ERROR");
		return message;
	}
	
	@ExceptionHandler(Exception.class)
	@ResponseBody
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public MessageDTO processDefaultException(Exception e) {
		MessageDTO message = new MessageDTO();
		message.setMessage(e.getClass().getName() + " caused by:" + e.getMessage());
		message.setType("ERROR");
		return message;
	}
	
}
