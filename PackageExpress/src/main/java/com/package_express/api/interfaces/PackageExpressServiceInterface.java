package com.package_express.api.interfaces;

import java.util.List;

public interface PackageExpressServiceInterface <T> {

	public T get(Integer id);
	public T save(T t);
	public T update(T t);
	public void delete(Integer id);
	public List<T> getAll();
	
	
}
