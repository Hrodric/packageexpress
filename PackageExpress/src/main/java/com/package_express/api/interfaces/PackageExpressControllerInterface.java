package com.package_express.api.interfaces;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public interface PackageExpressControllerInterface<T> {

	@GetMapping(path = "{id}")
	public T get(@PathVariable(name = "id") Integer id);

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public T save(@RequestBody T t);

	@PutMapping( consumes = MediaType.APPLICATION_JSON_VALUE)
	public T update(@RequestBody T t);

	@DeleteMapping(path = "{id}")
	public void delete(@PathVariable(name = "id") Integer id);

	@GetMapping
	public List<T> getAll();
}
