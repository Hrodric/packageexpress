package com.package_express.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.package_express.api.entity.Usuario;
import com.package_express.api.exception.ResourceNotFoundException;
import com.package_express.api.interfaces.PackageExpressServiceInterface;
import com.package_express.api.repository.UsuarioRepository;

@Service
public class UsuarioService implements PackageExpressServiceInterface<Usuario> {

	private static final String USU_ID = "usuId";
	private static final String USUARIO = "Usuario";
	UsuarioRepository usuarioRepo;

	@Autowired
	public void usuarioRepo(UsuarioRepository usuarioRepo) {
		this.usuarioRepo = usuarioRepo;
	}

	@Override
	public Usuario get(Integer id) {
		return this.usuarioRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException(USUARIO, USU_ID, id.toString()));
	}

	@Override
	public Usuario save(Usuario usuario) {
		return this.usuarioRepo.save(usuario);
	}

	@Override
	public Usuario update(Usuario usuario) {
		Usuario usuarioDb = usuarioRepo.findById(usuario.getUsuId())
				.orElseThrow((() -> new ResourceNotFoundException(USUARIO, USU_ID, usuario.getUsuId().toString())));

		usuarioDb.setOfiId(usuario.getOfiId());
		usuarioDb.setUsuApellido(usuario.getUsuApellido());
		usuarioDb.setUsuEstatus(usuario.getUsuEstatus());
		usuarioDb.setUsuNombre(usuario.getUsuNombre());
		usuarioDb.setUsuRoll(usuario.getUsuRoll());
		usuarioDb.setUsuPass(usuario.getUsuPass());
		return usuarioRepo.save(usuarioDb);
	}

	@Override
	public void delete(Integer id) {
		Usuario usuario = usuarioRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException(USUARIO, USU_ID, id.toString()));
		usuarioRepo.delete(usuario);

	}

	@Override
	public List<Usuario> getAll() { 
		return (List<Usuario>) usuarioRepo.findAll();
	}

}
