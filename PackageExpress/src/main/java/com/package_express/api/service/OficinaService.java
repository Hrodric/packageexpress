package com.package_express.api.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.package_express.api.entity.Oficina;
import com.package_express.api.exception.ResourceNotFoundException;
import com.package_express.api.interfaces.PackageExpressServiceInterface;
import com.package_express.api.repository.OficinaRepository;

@Service
public class OficinaService implements PackageExpressServiceInterface<Oficina> {

	private static final String OFI_ID = "ofiId";
	private static final String OFICINA = "Oficina";
	private OficinaRepository oficinaRepo;
	private Logger LOG = LoggerFactory.getLogger(OficinaService.class);

	@Autowired
	public void oficinaRepo(OficinaRepository oficinaRepo) {
		this.oficinaRepo = oficinaRepo;
	}

	@Override
	public Oficina get(Integer id) {
		return this.oficinaRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException(OFICINA, OFI_ID, id.toString()));
	}

	@Override
	public Oficina save(Oficina oficina) {
		return oficinaRepo.save(oficina);
	}

	@Override
	public Oficina update(Oficina oficina) {
		Oficina oficinaDb = oficinaRepo.findById(oficina.getOfiId())
				.orElseThrow((() -> new ResourceNotFoundException(OFICINA, OFI_ID, oficina.getOfiId().toString())));

		oficinaDb.setOfiCiudad(oficina.getOfiCiudad());
		oficinaDb.setOfiDescripcion(oficina.getOfiDescripcion());
		oficinaDb.setOfiEstado(oficina.getOfiEstado());
		oficinaDb.setOfiPais(oficina.getOfiPais());
		oficinaDb.setOfiZipCode(oficina.getOfiZipCode());
		return oficinaRepo.save(oficinaDb);
	}

	@Override
	public void delete(Integer id) {
		Oficina oficina = oficinaRepo.findById(id)
				.orElseThrow(((() -> new ResourceNotFoundException(OFICINA, OFI_ID, id.toString()))));
		oficinaRepo.delete(oficina);
	}

	@Override
	public List<Oficina> getAll() {
		return (List<Oficina>) this.oficinaRepo.findAll();
	}
}
